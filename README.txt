UTILISATION : 
Ordonnanceur.exe <nombre de threads> <RAM allouée en Ko> 

Le programme récupèrera les tâches écrites dans le fichier TaskList.txt et les exécutera en parallèle avec les ressources définies.
(On aurait pu ajouter un paramètre pour utiliser un fichier .txt custom mais bon)

Les tâches dans TaskList.txt sont écrites sous la forme <COMMANDE>;<MEMOIRE NECESSAIRE>;<TEMPS MAXIMAL>
Pendant l'exécution du Dispatcher, il est possible à tout moment d'ajouter des tâches dans la console principale. 
Les logs de l'exécution des tâches sont écrits dans la console secondaire.

Si la fenêtre Logs Dispatcher arrête d'afficher les nouvelles tâches, un appui sur la touche entrée devrait la débloquer.


NOTE : 
Ca compile pas sous Linux. 
On a tous travaillé sous Windows pendant le développement, du coup on a inévitablement dû utiliser des fonctions Microsoft parce que personne voulait changer d'OS...

NOTE 2 : 
Le timeout des tâches ne fonctionne pas.
On avait une implémentation, mais elle buggait beaucoup trop, du coup on est repassé à un truc plus simple à base de popen sans réimplémenter le timeout.