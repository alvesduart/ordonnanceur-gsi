#include "StrategyHighMemory.h"

int StrategyHighMemory::getNextTaskIndex(std::vector<Task> tasks, int availableMemory)
{
	int maxMemoryTask = tasks[0].getRAM();
	int maxMemoryIndex = 0;

	for (int i = 1; i< tasks.size(); i++)
	{
		if (tasks[i].getRAM() < availableMemory && tasks[i].getRAM() > maxMemoryTask)
		{
			maxMemoryTask = tasks[i].getRAM();
			maxMemoryIndex = i;
		}
	}

	return maxMemoryIndex;
}

