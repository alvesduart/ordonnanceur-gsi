#pragma once
#ifndef TASK_H
#define TASK_H

#include <iostream>

class Task
{
private:
	std::string command;
	int requiredMemory;
	int timeLimit;

public:
	Task(std::string cmd, int reqRAM, int time);

	std::string getCmd();
	int getRAM();
	int getTime();
};

#endif

