UTILISATION : 
Ordonnanceur.exe <nombre de threads> <RAM allou�e en Ko> 

Le programme r�cup�rera les t�ches �crites dans le fichier TaskList.txt et les ex�cutera en parall�le avec les ressources d�finies.
(On aurait pu ajouter un param�tre pour utiliser un fichier .txt custom mais bon)

Les t�ches dans TaskList.txt sont �crites sous la forme <COMMANDE>;<MEMOIRE NECESSAIRE>;<TEMPS MAXIMAL>
Pendant l'ex�cution du Dispatcher, il est possible � tout moment d'ajouter des t�ches dans la console principale. 
Les logs de l'ex�cution des t�ches sont �crits dans la console secondaire.

Si la fen�tre Logs Dispatcher arr�te d'afficher les nouvelles t�ches, un appui sur la touche entr�e devrait la d�bloquer.


NOTE : 
Ca compile pas sous Linux. 
On a tous travaill� sous Windows pendant le d�veloppement, du coup on a in�vitablement d� utiliser des fonctions Microsoft parce que personne voulait changer d'OS...

NOTE 2 : 
Le timeout des t�ches ne fonctionne pas.
On avait une impl�mentation, mais elle buggait beaucoup trop, du coup on est repass� � un truc plus simple � base de popen sans r�impl�menter le timeout.