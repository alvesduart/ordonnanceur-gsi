#include "Dispatcher.h"

void Dispatcher::setTaskList(std::vector<Task> tasks)
{
	taskList = tasks;
}

//Don't use those two while running tasks : It will fuck up the memory/thread allocation !
void Dispatcher::setMaxThreads(int maxT)
{
	maxThreads = maxT;
	availableThreads = maxT;
}

void Dispatcher::setMaxRAM(int maxRAM)
{
	maxMemory = maxRAM;
	availableMemory = maxRAM;
}


Strategy Dispatcher::getCurrentStrategy()
{
	return *currentStrat;
}

void Dispatcher::changeStrategy(Strategy *s)
{
	currentStrat = s;
}

int Dispatcher::canGetTask()
{
	//Are there any tasks available ?
	if (taskList.empty())
		return -3;

	//Can we run a task ?
	if (availableThreads <= 0)
		return -2;
	
	//Do we have enough memory to run the next task ?
	int taskIndex = currentStrat->getNextTaskIndex(taskList, availableMemory);

	if (taskIndex == -1) //If not enough memory available, give up
		return -1;

	//if none of the errors were returned, we're good to go
	return 0;
}

Task Dispatcher::getNextTask()
{
	//Get next task according to strategy
	int taskIndex = currentStrat->getNextTaskIndex(taskList, availableMemory);

	//Get it from list
	Task t = taskList[taskIndex];

	printLogConsoleColor("Strategy selected task " + t.getCmd() + "\n", CConsoleLoggerEx::COLOR_WHITE | CConsoleLoggerEx::COLOR_BACKGROUND_BLUE);

	//We're loading it, so we reserve system resources
	availableThreads -= 1;
	availableMemory = availableMemory - t.getRAM();

	//Delete it from list and return
	taskList.erase(taskList.begin() + taskIndex);
	return t;
}

bool Dispatcher::addTask(Task t)
{
	taskList.push_back(t);
	return true;
}

void Dispatcher::runTask(Task t)
{

	Worker w = Worker(t); //Creating a worker object with a task as argument automatically starts the task.

}

void Dispatcher::taskEnded(int memoryFreed)
{
	//When task ends
	availableMemory += memoryFreed;
	availableThreads += 1;
}

CConsoleLoggerEx Dispatcher::getConsole()
{
	return logConsole;
}

void Dispatcher::printLogConsole(std::string s)
{
		logConsole.printf(s.c_str());
}

//Variante de printLogConsole avec utilisation de couleurs ! C'est compl�tement inutile donc indispensable.
//Les couleurs sont des constantes de la forme CConsoleLoggerEx::COLOR_RED .
void Dispatcher::printLogConsoleColor(std::string s, int colors)
{

	logConsole.cprintf(colors, s.c_str());
	//On reset au blanc apr�s en imprimant un espace en COLOR_WHITE
	logConsole.cprintf(CConsoleLoggerEx::COLOR_WHITE, " ");
}



int Dispatcher::initConsole(char* title)
{
	return logConsole.Create(title);
}

Dispatcher::~Dispatcher()
{
}
