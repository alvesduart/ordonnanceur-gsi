#include <fstream>
#include <iostream>
#include <vector>
#include "Task.h"
#include "TaskReader.h"
#include "Dispatcher.h"
#include "StrategyHighMemory.h"
#include "ConsoleLogger.h"


using namespace std;

//Fonction pour ajouter manuellement une t�che � la liste pendant le d�roulement du Dispatcher
Task manualAdd()
{
	std::string cmd = "";
	std::string memory = "";
	std::string timeLimit = "";

	cout << "Vous pouvez entrer des taches manuellement." << endl;

	cout << "Entrez d'abord la commande que vous voulez passer" << endl;

	while (cmd == "")
		std::getline(cin, cmd);

	cout << "Entrez le cout en memoire de la tache (en Ko)" << endl;

	while (memory == "")
	std::getline(cin, memory);

	cout << "Entrez maintenant le temps limite pour l'execution (en ms)" << endl;

	while (timeLimit == "")
	std::getline(cin, timeLimit);


	return Task(cmd, std::stoi(memory), std::stoi(timeLimit));
}

int main(int argc, char *argv[])
{

	if (argc != 3) // On demande 2 arguments donc argc = 3
		cout << "Emploi: " << argv[0] << " <nombre de threads> <RAM allou�e en Ko>" << std::endl;
	else {

		//Construction de ma liste de t�ches via un fichier texte
		vector<Task> taskList = TaskReader("TaskList.txt").getTaskList();

		/*taskList.push_back(Task("dir", 64));
		taskList.push_back(Task("driverquery", 500));
		taskList.push_back(Task("netstat -an", 250));
		taskList.push_back(Task("ping eeisti.eistiens.net", 250));*/


		// argv[1] est le nombre de threads et argv[2] la RAM max, on initialise le Dispatcher
		cout << "Initialisation du Dispatcher..." << endl;
		
		//Appel du singleton dispatcher et param�trage
		int maxThreads = atoi(argv[1]);
		int maxRAM = atoi(argv[2]);

		Dispatcher::getInstance().setTaskList(taskList);
		Dispatcher::getInstance().setMaxThreads(maxThreads);
		Dispatcher::getInstance().setMaxRAM(maxRAM);
		Dispatcher::getInstance().initConsole("Logs Dispatcher"); //On ouvre la console du logger

		StrategyHighMemory strat; //On utilise HighMemory par d�faut.
		Dispatcher::getInstance().changeStrategy(&strat);

		bool tasksEnded = false; //switch pour terminer le programme quand la liste de t�ches a �t� compl�t�e
		bool messageNotPosted = true; 

		#pragma omp parallel sections
		{
			#pragma omp section
			{ 
				while (1) //Fen�tre 1: Interaction avec l'utilisateur pour l'ajout de t�ches
				{ 
					Task t = manualAdd();

					Dispatcher::getInstance().addTask(t);
					//Resetting des bool�ens qui cachent le spam de messages de la console
					tasksEnded = false;
					messageNotPosted = true;
				}
			}

			#pragma omp section
			{
				//Boucle de l'ordonnanceur avec parallelisation: chaque thread correspond � l'ex�cution d'une t�che.
				while (1) //Fen�tre 2: Affichage de l'ex�cution des t�ches
				{
					//omp parallel instancie une plage de threads qui vont tous ex�cuter le bloc de code ci-dessous
					#pragma omp parallel 
					{
						#pragma omp flush(tasksEnded)
						if (!tasksEnded)
						{

							Dispatcher::getInstance().printLogConsoleColor("Recuperation d'une tache...\n", CConsoleLoggerEx::COLOR_WHITE | CConsoleLoggerEx::COLOR_BACKGROUND_RED);
							Task c = Task("placeholder", 0, 0); //La t�che qui va �tre ex�cut�e
							int taskStatus = 0;

							#pragma omp critical(getTask) //Les threads r�cup�rent la t�che chacun leur tour
							{
								taskStatus = Dispatcher::getInstance().canGetTask(); //Peut-on obtenir une t�che ? 

								//Les codes d'erreur possibles sont les suivants 
								switch (taskStatus) {

								case -1:
									Dispatcher::getInstance().printLogConsoleColor("Pas assez de memoire disponible.\n", CConsoleLoggerEx::COLOR_WHITE | CConsoleLoggerEx::COLOR_BACKGROUND_RED);
									break;

								case -2:
									Dispatcher::getInstance().printLogConsoleColor("Pas de threads disponibles.\n", CConsoleLoggerEx::COLOR_WHITE | CConsoleLoggerEx::COLOR_BACKGROUND_RED);
									break;

								case -3:
									tasksEnded = true; //-3 = liste finie, on informe les autres threads avec la pragma flush 
									#pragma omp flush(tasksEnded)
									break;

								default:
									c = Dispatcher::getInstance().getNextTask();
									break;
								}
							}

							if (taskStatus == 0) //Si on a bien obtenu notre t�che, on l'ex�cute
								Dispatcher::getInstance().runTask(c);

						}
					}

					//Sortie des threads, on v�rifie si la liste a �t� trait�e dans son int�gralit�: si oui on arr�te de poll (jusqu'� ce que l'utilisateur ajoute des t�ches...)
					if (tasksEnded && messageNotPosted)
					{
						Dispatcher::getInstance().printLogConsoleColor("Liste de taches vide !\n", CConsoleLoggerEx::COLOR_WHITE | CConsoleLoggerEx::COLOR_BACKGROUND_BLUE);
						messageNotPosted = false; //Ptit flag pour ne pas r�imprimer liste des t�ches vide tout le temps
					}


				}
			}
		}

	}
}