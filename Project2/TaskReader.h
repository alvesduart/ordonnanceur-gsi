#pragma once
#ifndef TASKREAD_H
#define TASKREAD_H

#include <string>
#include <vector>
#include "Task.h"
class TaskReader
{

private:
	std::vector<Task> taskList;


public:
	TaskReader(std::string fileLocation);
	~TaskReader();

	std::vector<Task> getTaskList();
};

#endif