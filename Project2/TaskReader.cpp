#include "TaskReader.h"
#include "Task.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


TaskReader::TaskReader(std::string fileLocation)
{

	std::string ligne;
	std::string rest;
	std::string name;
	std::string memory;
	std::string timeLimit;
	
	std::vector<Task> tableau;
	size_t found;
	size_t found2;
	std::ifstream InFlux(fileLocation); //ouverture fichier lecture
	if (InFlux) {
		//lecture
		while (getline(InFlux, ligne)) {
			found = ligne.find_first_of(';');
			name = ligne.substr(0, found);
			rest = ligne.substr(found + 1);
			found2 = rest.find(';');
			memory = rest.substr(0, found2);
			timeLimit = rest.substr(found2 + 1);


			Task t = Task(name, std::stoi(memory), std::stoi(timeLimit));
			tableau.push_back(t);
		}

		//On stocke le tableau construit dans l'objet
		taskList = tableau;
		
	} else 
		std::cout << "ERREUR : Impossible d'ouvrir le fichier en lecture." << std::endl;
}


TaskReader::~TaskReader()
{
}

std::vector<Task> TaskReader::getTaskList()
{
	return taskList;
}
