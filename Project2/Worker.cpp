#include "Worker.h"


Worker::Worker(Task t)
{
	Dispatcher::getInstance().printLogConsoleColor("Executing Task " + t.getCmd() + "\n", CConsoleLoggerEx::COLOR_WHITE | CConsoleLoggerEx::COLOR_BACKGROUND_GREEN);

	std::string result = getStdoutFromCommand(t.getCmd());

	Dispatcher::getInstance().printLogConsole(result);

	//Fin, on annonce au dispatcher qu'on n'a plus besoin de la m�moire
	Dispatcher::getInstance().taskEnded(t.getRAM());

}

Worker::~Worker()
{

}


//Ex�cute une commande et r�cup�re son STDOUT dans un string
std::string Worker::getStdoutFromCommand(std::string cmd) {

	std::string data;
	FILE * stream;
	const int max_buffer = 256;
	char buffer[max_buffer];
	cmd.append(" 2>&1"); // Do we want STDERR?

	stream = _popen(cmd.c_str(), "r"); //popen = _popen sous windows. Pourquoi pas ?
	if (stream) {
		while (!feof(stream))
			if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
		_pclose(stream);
	}
	return data;
}
