#include "Task.h"

Task::Task(std::string cmd, int reqRAM, int time)
{
	command = cmd;
	requiredMemory = reqRAM;
	timeLimit = time;
}

std::string Task::getCmd()
{
	return command;
}

int Task::getRAM()
{
	return requiredMemory; 
}

int Task::getTime()
{
	return timeLimit;
}