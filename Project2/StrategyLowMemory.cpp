#include "StrategyLowMemory.h"

int StrategyLowMemory::getNextTaskIndex(std::vector<Task> tasks, int availableMemory)
{
	int minMemoryTask = tasks[0].getRAM();
	int minMemoryIndex = 0;

	for (int i = 1; i< tasks.size(); i++)
	{
		if (tasks[i].getRAM() < availableMemory && tasks[i].getRAM() < minMemoryTask)
		{
			minMemoryTask = tasks[i].getRAM();
			minMemoryIndex = i;
		}
	}

	return minMemoryIndex;
}