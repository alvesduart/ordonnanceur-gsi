#pragma once

#ifndef STRAT_H
#define STRAT_H

#include <iostream>
#include <vector>
#include "Task.h"

class Strategy
{

public:
	
	Strategy() {};
	~Strategy() {};
	
	//Get the index of the next task to process in the list 
	virtual int getNextTaskIndex(std::vector<Task> tasks, int availableMemory);

};

#endif

