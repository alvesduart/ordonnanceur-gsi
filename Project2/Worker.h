#pragma once

#ifndef WORKER_H
#define WORKER_H

#include <iostream>
#include "Task.h"
#include "Dispatcher.h"
#include <string>
#include <Windows.h>
#include <stdio.h>

#define BUFSIZE 4096

class Worker
{
	//Classe basique pour executer une t�che.

public:
	Worker(Task t); //On initialise avec un pointeur vers le dispatcher pour ne pas le copier
	~Worker();
	void ReadFromPipe(PROCESS_INFORMATION piProcInfo);
	PROCESS_INFORMATION Worker::CreateChildProcess(std::string cmd, int timeLimit);
	std::string getStdoutFromCommand(std::string cmd);

};

#endif