#include "Strategy.h"

int Strategy::getNextTaskIndex(std::vector<Task> tasks, int availableMemory)
{
	//Default implementation, always returns the first item of the list
	//Actual strategies are implemented with classes inheriting from Strategy and overriding this method

	if (tasks[0].getRAM() > availableMemory) //No task if not enough RAM
		return -1;
	else
		return 0; 
}
