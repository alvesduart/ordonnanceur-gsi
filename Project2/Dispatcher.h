#pragma once

#ifndef DISPATC_H
#define DISPATC_H

#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include "Strategy.h"
#include "Task.h"
#include "Worker.h"
#include "ConsoleLogger.h"

class Dispatcher
{

//Making this a singleton for easy access by workers
public:
	static Dispatcher& getInstance()
	{
		static Dispatcher instance; // Guaranteed to be destroyed.
							  // Instantiated on first use.
		return instance;
	}

private:
	Strategy *currentStrat;
	std::vector<Task> taskList;
	int maxThreads; //Maximum number of processes that can run simultaneously
	int maxMemory; //Max RAM available, in kilobytes

	int availableThreads; //Currently available process slots
	int availableMemory; //Amount of memory available

	CConsoleLoggerEx logConsole; //Handle vers le processus ou on envoie les logs

public:
	Dispatcher() {};

	void setTaskList(std::vector<Task> tasks);
	void setMaxThreads(int maxT);
	void setMaxRAM(int maxRAM);

	//Changes strategy used by dispatcher to hand Tasks to Workers
	Strategy getCurrentStrategy();
	void changeStrategy(Strategy *s);

	//Checks if we can call getNextTask.
	int canGetTask();

	//Get the next Task according to our strategy and take it off the list. 
	//Returns NULL if current resources aren't sufficient for a new Task.
	Task getNextTask();

	bool addTask(Task t);

	//Runs a task: Creates a worker and assigns resources to it.
	void runTask(Task t);

	//Called by a worker when his Task has ended. Frees the specified amounts of memory.
	void taskEnded(int memoryFreed);

	CConsoleLoggerEx getConsole();
	void printLogConsole(std::string s);
	void printLogConsoleColor(std::string s, int colors);
	int initConsole(char* title);

	~Dispatcher();

	//Deleting the regular ways to instanciate the object for true singleton behavior
	Dispatcher(Dispatcher const&) = delete;
	void operator=(Dispatcher const&) = delete;
};


#endif
