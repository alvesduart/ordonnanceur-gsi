#pragma once

#ifndef STRATLM_H
#define STRATLM_H

#include <iostream>
#include <vector>
#include "Task.h"
#include "Strategy.h"

class StrategyLowMemory : public Strategy
{
public:
	StrategyLowMemory() {};
	~StrategyLowMemory() {};
	int getNextTaskIndex(std::vector<Task> tasks, int availableMemory);
};
#endif