#pragma once

#ifndef STRATHM_H
#define STRATHM_H

#include <iostream>
#include <vector>
#include "Task.h"
#include "Strategy.h"

class StrategyHighMemory : public Strategy
{
public:
	StrategyHighMemory() {};
	~StrategyHighMemory() {};
	int getNextTaskIndex(std::vector<Task> tasks, int availableMemory);
};

#endif
